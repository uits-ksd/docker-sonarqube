FROM sonarqube:7.7-community
COPY conf/sonar.properties /opt/sonarqube/conf/
COPY conf/wrapper.conf /opt/sonarqube/conf/
COPY jdbc-driver/oracle/ojdbc8.jar /opt/sonarqube/extensions/jdbc-driver/oracle/
COPY jdbc-driver/oracle/* /opt/sonarqube/extensions/jdbc-driver/
